CREATE DATABASE empresa_cupones;
use empresa_cupones;

CREATE TABLE usuario(
	id_cliente int auto_increment not null,
	nombre varchar(100) not null,
	apellidos varchar(100) not null,
    email varchar(100) not null,
	contrasena varchar(20) not null,
	usuario_referido varchar(100),
	PRIMARY KEY(id_cliente),
	FOREIGN KEY (id_cliente)
    REFERENCES cupones(id_cliente)
);

CREATE TABLE cupones(
	id_cupon int auto_increment not null,
	id_cliente int not null,
	restaurante varchar(100) not null,
    rfc_restaurante varchar(100) not null,
	descripción varchar(100),
	fecha_caducidad date,
	PRIMARY KEY(id_cupon)
);
