package com.empresa.cupones.utils;

import java.util.ArrayList;
import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import com.empresa.cupones.dao.UsuarioEntity;
import com.empresa.cupones.dto.CuponesRequest;
import com.empresa.cupones.dto.CuponesResponse;
import com.empresa.cupones.dto.UsuarioRequest;

@Component
public class Utils {

	public ResponseEntity<CuponesRequest> validaGetUsuario(CuponesRequest request) {
		if (!request.isEstatus()) 
            return new ResponseEntity<CuponesRequest>(request, HttpStatus.BAD_REQUEST);
        else
        	return new ResponseEntity<CuponesRequest>(request, HttpStatus.OK);
    }
	
	public ResponseEntity<CuponesResponse> validaSaveUsuario(CuponesResponse response) {
		if (!response.isEstatus()) 
            return new ResponseEntity<CuponesResponse>(response, HttpStatus.BAD_REQUEST);
        else
        	return new ResponseEntity<CuponesResponse>(response, HttpStatus.OK);
    }
	
	public UsuarioEntity llenaEntity(UsuarioRequest request) {
		UsuarioEntity entity = new UsuarioEntity();
		entity.setNombre(request.getNombre());
		entity.setApellidos(request.getApellidos());
		entity.setEmail(request.getEmail());
		entity.setContrasena(request.getContrasena());
		entity.setUsuario_referido(request.getUsuarioReferido());	
		return entity;
	}
	
	public CuponesResponse llenaResponseSave(String mensaje, boolean estatus) {
		CuponesResponse response = new CuponesResponse();
		response.setMensaje(mensaje);
		response.setEstatus(estatus);
		return response;
	}
	
	public CuponesRequest llenaResponseGet(String mensaje, boolean estatus, List<UsuarioRequest> listaUsuario) {
		CuponesRequest response = new CuponesRequest();
		response.setMensaje(mensaje);
		response.setEstatus(estatus);
		response.setListaUsuario(listaUsuario);
		return response;
	}
	
	public List<UsuarioRequest> llenaUsuarioRequest(List<UsuarioEntity> lista) {
		List<UsuarioRequest> request  = new ArrayList<UsuarioRequest>();
		lista.stream().forEach((usuario)-> {
			UsuarioRequest response = new UsuarioRequest();
			response.setNombre(usuario.getNombre());
			response.setApellidos(usuario.getApellidos());
			response.setEmail(usuario.getEmail());
			response.setContrasena(usuario.getContrasena());
			response.setUsuarioReferido(usuario.getUsuario_referido());
			request.add(response);
		});
		return request;
	}
	
	
}
