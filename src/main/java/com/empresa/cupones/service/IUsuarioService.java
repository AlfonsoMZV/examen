package com.empresa.cupones.service;

import org.springframework.http.ResponseEntity;

import com.empresa.cupones.dao.UsuarioEntity;
import com.empresa.cupones.dto.CuponesRequest;
import com.empresa.cupones.dto.CuponesResponse;

public interface IUsuarioService {
	 
	ResponseEntity<CuponesRequest> obtieneUsuario();
	ResponseEntity<CuponesResponse> insertaUsuario(UsuarioEntity usuario);
	
}
