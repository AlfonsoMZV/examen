package com.empresa.cupones.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import org.apache.logging.log4j.*;

import com.empresa.cupones.dao.UsuarioEntity;
import com.empresa.cupones.dto.CuponesRequest;
import com.empresa.cupones.dto.CuponesResponse;
import com.empresa.cupones.dto.UsuarioRequest;
import com.empresa.cupones.repository.UsuarioRepository;
import com.empresa.cupones.service.IUsuarioService;
import com.empresa.cupones.utils.Utils;

@Service
public class UsuarioService implements IUsuarioService{
	
	private static Logger LOGG = LogManager.getLogger(UsuarioService.class);
	
	@Autowired
	private UsuarioRepository usuarioRepository;

	@Autowired
	private Utils utils;
	
	@Override
	public ResponseEntity<CuponesRequest> obtieneUsuario() {
		ResponseEntity<CuponesRequest> response = null;
		List<UsuarioEntity> lista =new ArrayList<UsuarioEntity>();
		List<UsuarioRequest> listaRequest = new ArrayList<UsuarioRequest>();
		CuponesRequest cuponesRequest = new CuponesRequest();
		try{
			lista = (List<UsuarioEntity>) usuarioRepository.findAll();
			listaRequest = utils.llenaUsuarioRequest(lista);
			cuponesRequest = utils.llenaResponseGet(null, true, listaRequest);
			response = utils.validaGetUsuario(cuponesRequest);
		}catch(Exception e) {
			LOGG.error("Error al guardar usuario save() UsuarioService => ", e.getMessage());
			cuponesRequest = utils.llenaResponseGet(null, true, listaRequest);
			response = utils.validaGetUsuario(cuponesRequest);
		}
		return response ;
	}

	@Override
	public ResponseEntity<CuponesResponse> insertaUsuario(UsuarioEntity usuario) {
		ResponseEntity<CuponesResponse> response = null;
		CuponesResponse cuponesResponse = new CuponesResponse();
		try{
			usuarioRepository.save(usuario);
			cuponesResponse = utils.llenaResponseSave("Usuario insertado", true);
			response = utils.validaSaveUsuario(cuponesResponse);
		}catch(Exception e) {
			LOGG.error("Error al guardar usuario save() UsuarioService => ", e.getMessage());
			cuponesResponse = utils.llenaResponseSave("Error al guardar usuario save() UsuarioService => " + e.getMessage(), false);
			response = utils.validaSaveUsuario(cuponesResponse);
		}
		
		return response;
	}

}
