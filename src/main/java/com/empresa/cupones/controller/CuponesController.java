package com.empresa.cupones.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.empresa.cupones.dto.CuponesRequest;
import com.empresa.cupones.dto.CuponesResponse;
import com.empresa.cupones.dto.UsuarioRequest;
import com.empresa.cupones.service.IUsuarioService;
import com.empresa.cupones.utils.Utils;

@RestController
public class CuponesController {

	@Autowired
	private IUsuarioService usuarioService; 
	
	@PostMapping(value="/insertaUsuario")
	public ResponseEntity<CuponesResponse> insertaAlertas(@RequestBody UsuarioRequest request) {
		return usuarioService.insertaUsuario(new Utils().llenaEntity(request));
	}
	
	@GetMapping(value="/getUsuarios")
	public ResponseEntity<CuponesRequest> consultaAlertas() {
		return usuarioService.obtieneUsuario();
	}
	
}
