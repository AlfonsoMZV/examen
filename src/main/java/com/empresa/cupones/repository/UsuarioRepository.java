package com.empresa.cupones.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.empresa.cupones.dao.UsuarioEntity;

@Repository
public interface UsuarioRepository extends CrudRepository < UsuarioEntity, Integer >{

}
