package com.empresa.cupones.dto;

import java.io.Serializable;
import java.util.List;

public class CuponesRequest implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	private String mensaje;
	private boolean estatus;
	private List<UsuarioRequest> listaUsuario;
	
	public String getMensaje() {
		return mensaje;
	}
	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}
	public boolean isEstatus() {
		return estatus;
	}
	public void setEstatus(boolean estatus) {
		this.estatus = estatus;
	}
	public List<UsuarioRequest> getListaUsuario() {
		return listaUsuario;
	}
	public void setListaUsuario(List<UsuarioRequest> listaUsuario) {
		this.listaUsuario = listaUsuario;
	}
	
	
}
