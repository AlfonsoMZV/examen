package com.empresa.cupones.dto;

import java.io.Serializable;

public class UsuarioRequest implements Serializable{
	private static final long serialVersionUID = 1L;
	
	private String nombre;
	private String apellidos;
	private String email;
	private String contrasena;
	private String usuarioReferido;

	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getApellidos() {
		return apellidos;
	}
	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getContrasena() {
		return contrasena;
	}
	public void setContrasena(String contrasena) {
		this.contrasena = contrasena;
	}
	public String getUsuarioReferido() {
		return usuarioReferido;
	}
	public void setUsuarioReferido(String usuarioReferido) {
		this.usuarioReferido = usuarioReferido;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
}
